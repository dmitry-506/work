//поиск и выбор контрагента
$(function(){
    
    //Живой поиск
    $('#name').on("change keyup input click", function() {

        if(this.value.length >= 2){

            var name = $(this).val();

            $.ajax({
                url: 'actions.php?ajax=search&&name='+name,
                dataType: 'json',

                success: function(data){

                    if (data.list != '') {

                    	$(".search_result").html(data.list).fadeIn();
                    	$("#add_button").addClass("hidden");

                    } else {
                    	
                    	$("#add_button").removeClass("hidden");
                    	$(".search_result").fadeOut();//прячем список

                    }
               },
            });
        }
    })
        
    $(".search_result").hover(function(){
        $("#name").blur(); //Убираем фокус с input
    })

    //При выборе результата поиска, прячем список и заносим выбранный результат в input
    $(".search_result").on("click", "li", function(){

        var name = $(this).text();

        $(".search_result").fadeOut();
        
            $.ajax({
            url: 'actions.php?ajax=select&&name='+name,
            dataType: 'json',
            success: function(data){
            
            	var contragent_id = data.id;
            	var name = data.name;

                $('#name').val(name);
                $('#contragent_id').val(contragent_id);
                
            }
        });
    })
});


//добавление нового контрагента
function add(){
	
	var name = $('#name').val();

	$.ajax({
        url: 'actions.php?ajax=add&&name='+name,
        dataType: 'json',
        success: function(data){

			var name = data.name;
			var contragent_id = data.id;

			$('#name').val(name);
			$('#contragent_id').val(contragent_id);
			$("#add_button").addClass("hidden");//прячем кнопку
			$(".search_result").fadeOut();//прячем список
		}

    });
    
}