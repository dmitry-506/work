<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Тендер</title>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/my.js"></script>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
	<script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
	<![endif]-->
</head>
<body>

<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<hr>
				
				<div class="col-lg-9">
					<input id="name" type="text" name="name" placeholder="Название" class="form-control input-lg">
					<input type="hidden" name="contragent_id" id="contragent_id">
				</div>
				
				<div class="col-lg-3" id="add_block">
					<button id='add_button' class='btn btn-lg btn-width btn-success hidden' onclick='add()'>Добавить</button>
				</div>
				
				<div class="col-lg-12">
					<ul class="search_result"></ul>
				</div>

			</div>
		</div>
	</div>
</div>

</body>
</html>